<?php
namespace switchBackup;

set_include_path(getcwd());

/**
 * Cisco Switch/Router Backup Class.
 */
class ciscorouter extends core {

    /**
     * Cisco Switch/Router backup wrapper.
     *
     * @return this chain.
     */
    public function backup(){

        // Check to see if it is alive.
        if(!$this->isAlive($this->switch, 22)){

            return $this; 

        }

        $this->runBackup();

        sleep(20);

        $this->moveBackups($this->storageLocation . '/' . $this->switch);

        return $this;

    }

    /**
     * Backup switch using SSH.
     *
     * @return void
     */
    public function runBackup(){

        
        $ssh = new \phpseclib\Net\SSH2($this->switch);

        if (!$ssh->login($this->username, $this->password)) {

            echo "Login Failed \n";
            return;

        }


        $command = "copy running-config tftp://" . $this->tftpIP . "/running.cfg" . PHP_EOL;
        $command .= PHP_EOL;
        $command .= PHP_EOL;
        $command .= PHP_EOL;
        $command .= "copy startup-config tftp://" . $this->tftpIP . "/startup.cfg" . PHP_EOL;
        $command .= PHP_EOL;
        $command .= PHP_EOL;
        $command .= PHP_EOL;

    
        return $ssh->exec($command);

    }



}
