<?php

namespace switchBackup;

/**
 * Dell Switch Backup Class.
 */
class dell extends core {

    /**
     * Main Telnet Backup Wrapper.
     *
     * @return this chain.
     */
    public function backup(){

        // Check to see if it is alive.
        if(!$this->isAlive($this->switch, 23)){

            return $this; 

        }

        $this->runBackup();

        $this->moveBackups($this->storageLocation . '/' . $this->switch);

        return $this;

    }

    /**
     * Run the backup of the switch against the TFTP server.
     *
     * @return string logs of shell_exec($command);
     */
    public function runBackup(){

        // exec $((sleep 3; echo ${USER};sleep 2;echo ${PASS};
        //sleep 2;echo copy running-config tftp://${BACKUPSERVER}/${SWITCHIP}.running.cfg;
        //sleep 60;echo copy startup-config tftp://${BACKUPSERVER}/${SWITCHIP}.startup.cfg;sleep 60)
        // | telnet ${SWITCHIP}${DOMAIN} &>/dev/null ) &>/dev/null &

        $command = "bash -c '$((";
        $command .= "sleep 3;";
        $command .= "echo " . $this->username . ";";
        $command .= "sleep 3;";
        $command .= "echo " . $this->password . ";";
        $command .= "sleep 3;";
        $command .= "echo copy running-config tftp://" . $this->tftpIP . "/running.cfg;";
        $command .= "sleep 30;";
        $command .= "echo copy startup-config tftp://" . $this->tftpIP . "/startup.cfg;";
        $command .= "sleep 30)";
        $command .= " | telnet " . $this->switch . " )' ";
        $command .= ' 2>&1; echo $?';


        return shell_exec($command);

    }

}