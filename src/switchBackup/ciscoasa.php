<?php

namespace switchBackup;

use phpseclib\Net\SSH2;

/**
 * Cisco Switch/Router Backup Class.
 */
class ciscoasa extends core {

    

    /**
     * Main SSH Backup Wrapper.
     *
     * @return this chain.
     */
    public function backup(){

        // Check to see if it is alive.
        if(!$this->isAlive($this->switch, 22)){

            return $this; 

        }

        $this->runBackup();

        $this->moveBackups($this->storageLocation . '/' . $this->switch);

        return $this;

    }

    public function runBackup(){

        $ssh = new \phpseclib\Net\SSH2($this->switch);

        if (!$ssh->login($this->username, $this->password)) {

            echo "Login Failed \n";
            return;

        }

        $command = 'login' . PHP_EOL;
        $command .= $this->login_user . PHP_EOL;
        $command .= $this->login_pass . PHP_EOL;
        $command .= 'copy running-config tftp://' . $this->tftpIP . '/running.cfg' . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL;
        $command .= 'copy startup-config tftp://' . $this->tftpIP . '/startup.cfg' . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL;
        $command .= 'exit' . PHP_EOL;


        return $ssh->exec($command);

    }


}