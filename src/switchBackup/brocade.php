<?php

namespace switchBackup;

/**
 * Brocade Switch/Router Backup Class.
 */
class brocade extends core {

    /**
     * Brocade Switch/Router backup wrapper.
     *
     * @return this chain.
     */
    public function backup(){

        // Check to see if it is alive.
        if(!$this->isAlive($this->switch, 22)){

            return $this; 

        }

        $this->runBackup();

        // Brocades don't hold wait until command finishes..
        sleep(10);

        $this->moveBackups($this->storageLocation . '/' . $this->switch);

        return $this;

    }

    /**
     * Backup switch using SSH.
     *
     * @return void
     */
    public function runBackup(){

        $logging = null;
        
        $ssh = new \phpseclib\Net\SSH2($this->switch);

        if (!$ssh->login($this->username, $this->password)) {

            echo "Login Failed \n";
            return;

        }

        $logging .= $ssh->read();
        $ssh->write("enable\n");
        $logging .= $ssh->read();
        $ssh->write($this->enablePassword . "\n");
        $logging .= $ssh->read();
        $ssh->write("copy running-config tftp " . $this->tftpIP . " running.cfg\n");
        $logging .= $ssh->read();
        $ssh->write("copy startup-config tftp " . $this->tftpIP . " startup.cfg\n");
        $logging .= $ssh->read();

        return $logging;

    }

}