<?php

namespace switchBackup;

/**
 * The Core class for backing up switches.
 */
class core {

    protected $username = null;
    protected $password = null;
    protected $enablePassword = null;
    protected $storageLocation = null;
    protected $tftpdStorage = "/srv/tftp";
    protected $switch = null;
    protected $tftpIP = null;
    protected $login_user = null;
    protected $login_pass = null;



    /**
     * Set the special additional login. Used in ASA.
     * 
     * @param string $user to use.
     * @param string $pass to use.
     * @return this Chain
     * 
     */
    public function setLogin(string $user, string $pass){


        $this->login_user = $user;
        $this->login_pass = $pass;

        return $this;

    }


    /**
     * Set the Username to use.
     *
     * @param string $username to use.
     * @return this Chain
     */
    public function setUsername(string $username){

        $this->username = $username;

        return $this;

    }

    /**
     * Set Password
     *
     * @param string $password to use.
     * @return this Chain
     */
    public function setPassword(string $password){

        $this->password = $password;

        return $this;

    }

    /**
     * Set Enable Password
     *
     * @param string $password to use for enable.
     * @return this Chain
     */
    public function setEnablePassword(string $password){

        $this->enablePassword = $password;

        return $this;

    }

    /**
     * Set the location to store files.
     *
     * @param string $path to storage.
     * @return this chain
     */
    public function setStorage(string $path){

        $this->storageLocation = $path;

        return $this;

    }

    /**
     * Set the Switch/Router we want to backup.
     *
     * @param string $switch dns or ip address.
     * @return this chain
     */
    public function setSwitch(string $switch){

        $this->switch = $switch;

        return $this; 

    }

    /**
     * Move Backup files to the correct loction.
     *
     * @param string $location location to store the files.
     * @return string logs of actions.
     */
    function moveBackups(string $location){

       $logs = shell_exec('mkdir -p ' . $location);

        $logs .= shell_exec('mv ' . $this->tftpdStorage . '/* ' . $location . ' 2>&1; echo $?');

        return $logs;

    }

    /**
     * Set this servers IP.
     *
     * @param [type] $ip address.
     * @return this chain.
     */
    public function setTftpIP($ip){

        $this->tftpIP = $ip;

        return $this;

    }
    
    /**
     * Check to see if the device is online.
     *
     * @param string $ip address of the device you want to check
     * @param integer $port that you want to check. 22 for ssh, 23 for telnet.
     * @return boolean
     */
    public function isAlive(string $ip, int $port){

        $fp = @fsockopen($ip, $port, $errno, $errstr, 0.1);

        if (!$fp) {

            return false;

        } 
        else {

            fclose($fp);

            return true;

        }

    }

}